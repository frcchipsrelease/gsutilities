/*******************************************************************************
 *
 * File: DataLogger.cpp
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 * 
 ******************************************************************************/
#include "gsutilities/DataLogger.h"
#include "gsutilities/Advisory.h"

#include <stdio.h>

namespace gsu
{

/*******************************************************************************
 *
 * @brief   create a data buffer of the specified size
 * 
 * This will crate an instance of a DataBuffer, the size of the buffer will
 * either be the size specified (1024 bytes if not specified) or zero if 
 * space for the buffer could not be allocated.
 * 
 * @param   max_size the maximum amount of data that this buffer can hold in 
 *                   bytes, default 1024 bytes
 * 
 ******************************************************************************/
DataBuffer::DataBuffer(uint16_t max_size)
{
    data = (uint8_t*)malloc(max_size);
    if(data != nullptr)
    {
        max_data_size = max_size;
    }
    else
    {
        max_data_size = 0;
    }
}

/*******************************************************************************
 *
 * @brief release the memory being held for this buffer
 * 
 ******************************************************************************/
DataBuffer::~DataBuffer(void)
{
    if (data != nullptr)
    {
        free(data);
        data = nullptr;
    }
}

/*******************************************************************************
 *
 * @return  the maximum amount of data that can be stored in this buffer
 * 
 ******************************************************************************/
uint16_t DataBuffer::getBufferSize(void)
{
    return max_data_size;
}


/*******************************************************************************
 *
 * @brief   create an instance of a DataLogger
 * 
 * A DataLogger uses a SegmentedFile, RecycleQueue, and thread to write
 * blocks of text representations of data to multiple segmented files. 
 * 
 * @param   path    the fully qualified path to all of the file segments
 *                  and index files used by Segmented File
 * 
 * @param   name    the base name for the segmented files, the complete name
 *                  includes the text passed to the open() method
 * 
 * @param   ext     the file extension for all of the data files
 * 
 * @param   max_segments    the maximum number of segments that will be
 *                          held for any given segmented file -- so if 
 *                          open() is called with three different strings
 *                          there will be 3*max_segments data files held
 * 
 * @param   max_buffers     the maximum number of data blocks that will be
 *                          held in memory to hold data while the blocks are
 *                          being written to file
 * 
 * @param   buffer_size     the number of bytes that will allocated for each 
 *                          block of data, this is the limit on how much
 *                          data can be written at one time
 * 
 ******************************************************************************/
DataLogger::DataLogger(std::string path, std::string name, std::string ext, uint8_t max_segments, 
    uint8_t max_buffers, uint16_t buffer_size)
    : queue(max_buffers, false)
    , file(path, name, ext, max_segments)
    , file_segment(nullptr)
    , logging_done(false)
    , max_buffer_size(buffer_size)
    , drain_thread(&DataLogger::drainMethod, this)
    , filename_base(name)
{
}

/*******************************************************************************
 *
 * @brief release any resources used by an instance of this class
 * 
 ******************************************************************************/
DataLogger::~DataLogger(void)
{
    close();

    logging_done = true;
    drain_thread.join();
}

/*******************************************************************************
 *
 * @brief open a segment of a SegmentedFile for writing data
 * 
 * @param   phase_name   the phase_name will be added to the DataLoggers
 *                       base name, the resulting name will be used to
 *                       open a new segemtn of a file, and delete an 
 *                       old segment if needed
 * 
 ******************************************************************************/
void DataLogger::open(const char* phase_name)
{
    close(); // just in case a file segment is open

    Advisory::postInfo("opening data logger file");
    if (! file.createPath())
    {
        Advisory::postCaution("failed to create data logger directory %s", file.getPath().c_str());
    }
    else
    {
        Advisory::postInfo("path exists %s", file.getPath().c_str());
    }

    file.setName(filename_base+phase_name);
    file_segment = file.openNextSegment();
}

/*******************************************************************************
 *
 * @brief   close the currently open file segment (if there is one)
 * 
 * If there is an open segment, any data held in the queue will be written 
 * to that open segment before the open segment is closed
 * 
 ******************************************************************************/
void DataLogger::close(void)
{
    if (file_segment != nullptr)
    {
        while (queue.getFullAvailable() > 0)
        {
            DataBuffer* db = queue.getFull(250);
            if (db != nullptr)
            {
                if (file_segment != nullptr)
                {
                    fprintf(file_segment, "%s", (char*)(db->data));
                }
                queue.putEmpty(db);
            }        
        }

        fclose(file_segment);
        file_segment = nullptr;
    }
}

/*******************************************************************************
 *
 * @return the maximum size of buffer segemnts that will be created by
 *         this instance
 * 
 ******************************************************************************/
uint16_t DataLogger::getBufferSize(void)
{
    return max_buffer_size;
}

/*******************************************************************************
 *
 * @brief   get a buffer that is ready for data to be written into
 * 
 * This will return a pointer to a DataBuffer that should have the size
 * specified in the constructor; however, if the allocation of memory 
 * failed, the DataBuffer will have a size of zero -- the data size should
 * be checked by anything using the returned buffer.
 * 
 * @return a pointer to a DataBuffer
 * 
 ******************************************************************************/
DataBuffer* DataLogger::getEmptyBuffer(void)
{
    //
    // The queue is set to not create buffers so this can create buffers
    // with the correct size
    //
    DataBuffer* buffer = queue.getEmpty();
    if (buffer == nullptr)
    {
        buffer = new DataBuffer(max_buffer_size);
    }

    return buffer;
}
           
/*******************************************************************************
 *
 * @brief   put/write a buffer full of data, this actually puts the buffer
 *          into a queue so it can be written without impacting the calling
 *          thread
 * 
 * @param   buffer  a pointer to a DataBuffer that is ready to be written
 *                  to file, this buffer should be null terminated
 *  
 ******************************************************************************/
void DataLogger::putFullBuffer(DataBuffer* buffer)
{
    queue.putFull(buffer);
}

/*******************************************************************************
 * 
 * @brief   write any buffers in the queue to the file
 * 
 * This method is used as the main method of a thread, it will wait for 
 * data to be added to the queue, then will write that data to the open file.
 * If there is not an open file, the data will be discarded.
 * 
 ******************************************************************************/
void DataLogger::drainMethod(void)
{
    while (! logging_done)
    {
        DataBuffer* db = queue.getFull(250);
        if (db != nullptr)
        {
           if (file_segment != nullptr)
            {
	            fprintf(file_segment, "%s", (char*)(db->data));
            }
            queue.putEmpty(db);
        }
    }
}

} // namespace gsu
