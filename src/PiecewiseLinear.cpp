/***************************************************************************//**
 *
 * @File PiecewiseLinear.cpp
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 * 
 ******************************************************************************/
#include "gsutilities/PiecewiseLinear.h"
#include <cmath>

namespace gsu
{

/***************************************************************************//**
 *
 * This no argument constructor is provided so an instance of this class can
 * be created before the curve data is defined. The no argument constructor
 * will create a Piecewise linear curve with 1 segment that will always result
 * in the evaluated value returning the argument to evaluate.
 *
 ******************************************************************************/
PiecewiseLinear::PiecewiseLinear(void)
{
    setCurve(std::vector<double>(), std::vector<double>());
    m_limit_ends = false;
}

/***************************************************************************//**
 *
 * Create an instance of a PiecewiseLinear class that can be used to
 * evaluate data points.
 *
 * The user must make sure there are at least 2 points
 * The user must make sure points are in order from smallest x to largest x
 *
 * @param	x		- an array of the x values of the points
 * @param	y		- an array of the y values of the points
 * @param   points	- the number of points that define the curve
 *
 ******************************************************************************/
PiecewiseLinear::PiecewiseLinear(double x[], double y[], uint16_t points)
{
    setCurve(x, y, points);
    m_limit_ends = false;
}

/***************************************************************************//**
 *
 * Create an instance of a PiecewiseLinear class that can be used to
 * evaluate data points.
 *
 * The user must make sure there are at least 2 points
 * The user must make sure points are in order from smallest x to largest x
 *
 * @param	x		- a vector of the x values of the points
 * @param	y		- a vector of the y values of the points
 *
 ******************************************************************************/
PiecewiseLinear::PiecewiseLinear(const std::vector<double>& x, const std::vector<double>& y)
{
    setCurve(x, y);
    m_limit_ends = false;
}

/***************************************************************************//**
 *
 * A copy constructor that will allow for a deep copy of a PieceWiseLinear
 * object.
 *
 * @param rh	- a reference to the right hand side of the assignment (the 
 *				  object to copy).
 *
 ******************************************************************************/
PiecewiseLinear::PiecewiseLinear(const PiecewiseLinear &rh)
{
    m_points = rh.m_points;
    m_points_x = rh.m_points_x;
    m_points_y = rh.m_points_y;
    m_limit_ends = rh.m_limit_ends;
}

/***************************************************************************//**
 *
 * An assignment constructor that will allow for a deep copy of a PieceWiseLinear
 * object.
 *
 * @param assign_pl	- a reference to the right hand side of the assignment (the 
 *				  object to copy).
 *
 ******************************************************************************/
PiecewiseLinear& PiecewiseLinear::operator=(const PiecewiseLinear& assign_pl)
{
    m_points = assign_pl.m_points;
    m_points_x = assign_pl.m_points_x;
    m_points_y = assign_pl.m_points_y;
    m_limit_ends = assign_pl.m_limit_ends;
    return *this;
}

/***************************************************************************//**
 *
 * Release the arrays used to hold the points on the curve.
 *
 ******************************************************************************/
PiecewiseLinear::~PiecewiseLinear(void)
{
    m_points_x.clear();
    m_points_y.clear();
    m_points = 0;
}

/***************************************************************************//**
 *
 * Set the curve with two arrays. This method creates two vectors and calls
 * the setCurve() method that takes two vectors.
 *
 * The user must make sure the x and y arrays have the same number of points.
 *
 * The user must make sure the points argument correctly identifies the number
 * of points in the arrays
 *
 * The user must make sure the values of x are in order from smallest to
 * largest
 *
 * @param   x       the array of x values
 * @param   y       the array of y values
 * @param   points  the number of points in the arrays/curve
 *
 ******************************************************************************/
void PiecewiseLinear::setCurve(double x[], double y[], uint16_t points)
{
    setCurve(std::vector<double>(x, x + points), std::vector<double>(y, y + points));
}

/***************************************************************************//**
 *
 * Set the curve with two vectors.
 *
 * If empty vectors are specified, a one segment curve will be created that
 * always results in evaluate return the same value that is specified as
 * an argument.
 *
 * If vectors with 1 point are specified, a one segment curve will be created
 * that always results in evaluate returning the value in the y vector.
 *
 * The user must make sure the x and y arrays have the same number of points.
 *
 * The user must make sure the values of x are in order from smallest to
 * largest
 *
 * @param   x       the array of x values
 * @param   y       the array of y values
 *
 ******************************************************************************/
void PiecewiseLinear::setCurve(const std::vector<double>& x, const std::vector<double>& y)
{
    m_points = (uint16_t) x.size();
    if (m_points == 0)
    {
        double temp_x[] = { 0.0, 1.0 };
        double temp_y[] = { 0.0, 0.0 };

        m_points = 2;
        m_points_x = std::vector<double>(temp_x, temp_x + 2);
        m_points_y = std::vector<double>(temp_y, temp_y + 2);
    }
    else if (m_points == 1)
    {
        double temp_x[] = { 0.0, 1.0 };
        double temp_y[] = { y[0], y[0] };

        m_points = 2;
        m_points_x = std::vector<double>(temp_x, temp_x + 2);
        m_points_y = std::vector<double>(temp_y, temp_y + 2);
    }
    else
    {
        m_points_x = x;
        m_points_y = y;
    }

}

/***************************************************************************//**
 *
 * Change the x and y values of one point on the curve.
 *
 * @param   index   the index of the point, from 0 to the the number of
 *                  points - 1, if the index is outside of the allowable
 *                  range this call will do nothing.
 *
 * @param   x_val   the new x value for the point. The value must be between
 *                  the x value of the previous point and the next point, if
 *                  it is not this call will do nothing.
 *
 * @param   y_val   the new y value for this point.
 *
 * @return true if the point is updated
 *
 ******************************************************************************/
bool PiecewiseLinear::setCurvePoint(uint16_t index, double x_val, double y_val)
{
    if (index < m_points)
    {
        if (index == 0)
        {
            if (x_val < m_points_x[1])
            {
                m_points_x[0] = x_val;
                m_points_y[0] = y_val;
                return true;
            }
        }
        else if (index == m_points - 1)
        {
            if (x_val > m_points_x[m_points - 2])
            {
                m_points_x[m_points - 1] = x_val;
                m_points_y[m_points - 1] = y_val;
                return true;
            }
        }
        else
        {
            if ((x_val > m_points_x[index - 1] ) && (x_val < m_points_x[m_points + 1]))
            {
                m_points_x[index] = x_val;
                m_points_y[index] = y_val;
                return true;
            }
        }
    }
    return false;
}

/***************************************************************************//**
 *
 * Set the end points to be limits. In otherwords, if asked to evaluate a
 * point that is less than the first point in the curve, the value of the
 * first point in the curve will be returned, if asked to evaluate a point
 * that is greater than the last point in the curve, the value of the last
 * point in the curve will be returned.
 *
 * @param   limit   if true, evaluation of points ourside of the curve will
 *                  result in the endpoints of the curve, if false the
 *                  last segements of the curve will be extended linearly
 *                  to infinity. The internal value for this is initialized
 *                  as false, the default when calling this method is true.
 *
 ******************************************************************************/
void PiecewiseLinear::setLimitEnds(bool limit)
{
    m_limit_ends = limit;
}

/***************************************************************************//**
 *
 * Evaluate a single point on the curve.
 *
 * @param x	- the value to evalueate
 *
 * @return the y value for the given x
 *
 ******************************************************************************/
double PiecewiseLinear::evaluate(double x) const
{
    if (m_limit_ends)
    {
        if (x < m_points_x[0])
        {
            return m_points_y[0];
        }
        else if ( x > m_points_x[m_points - 1])
        {
            {
                return m_points_y[m_points - 1];
            }
        }
    }

    for (int i = 1; i < m_points; i++)
    {
        if (x < m_points_x[i])
        {
            return evaluate(x, m_points_x[i - 1], m_points_y[i - 1], m_points_x[i], m_points_y[i]);
        }
    }

    return evaluate(x, m_points_x[m_points - 2], m_points_y[m_points - 2], m_points_x[m_points - 1], m_points_y[m_points - 1]);
}

/***************************************************************************//**
 *
 * This private method is used to evaluate a point on a given segment of the
 * curve.
 *
 * @param x		- the value to evaluate
 * @param x0	- the x value of the first point that defines the segment
 * @param y0	- the y value of the first point that defines the segment
 * @param x1	- the x value of the second point that defines the segment
 * @param y1	- the y value of the second point that defines the segment
 *
 * @return the y value for the provided x
 *
 ******************************************************************************/
inline double PiecewiseLinear::evaluate(double x, double x0, double y0, double x1, double y1)
{
    // y = (m * x) + b
    // m = (y1 - y0) / (x1 - x0)
    // b = y0 - (m * x0)
    // y = (m * x) + y0 - (m * x0)
    //   = (m * (x - x0)) + y0
    //   = (((y1 - y0) / (x1 - x0)) * (x - x0)) + y0;
    return (((y1 - y0) / (x1 - x0)) * (x - x0)) + y0;
}

}

