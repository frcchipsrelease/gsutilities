/*******************************************************************************
 *
 * File: Filter.cpp
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 * 
 ******************************************************************************/

// TODO: Write explanations for functions?

#include "gsutilities/Filter.h"
#include <cmath>

namespace gsu
{
/***************************************************************************//**
 *
 ******************************************************************************/
double Filter::limit(double value, double min, double max)
{
    if (max > min)
    {
		if (value > max)
		{
			return max;
		}
		else if (value < min)
		{
			return min;
		}
        else
        {
            return value;
        }
    }

    else if (min > max)
    {
        if (value > min)
		{
			return min;
		}
		else if (value < max)
		{
			return max;
		}
        else
        {
            return value;
        }
    }

    else
    {
        return max;
    }
}

/*******************************************************************************
 *
 ******************************************************************************/

// Works in theory but not yet tested
double Filter::step(double value, double size, double target)
{
    if (value < target)
    {
        if (size < (target - value))
        {
            return value + size;
        }
        else
        {
            return target;
        }
    }
    else if (value > target)
    {
        if (size < (value - target))
        {
            return value - size;
        }
        else
        {
            return target;
        }
    }
    else
    {
        return target;
    }

}
} //closing brace for namespace