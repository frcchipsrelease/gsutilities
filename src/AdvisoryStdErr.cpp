/*******************************************************************************
 *
 * File: AdvisoryStdOut.cpp
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 * 
 ******************************************************************************/
#include "gsutilities/AdvisoryStdErr.h"

/*******************************************************************************
 *
 * The private constructor will prevent instances of this class from being
 * created out of context, AdvisoryStdErr::init() should be used instead.
 *
 ******************************************************************************/
AdvisoryStdErr::AdvisoryStdErr(void) : Advisory()
{
}

/*******************************************************************************
 *
 * The virtual destructor allows for all clean up
 *
 ******************************************************************************/
AdvisoryStdErr::~AdvisoryStdErr(void)
{
}

/*******************************************************************************
 *
 * This method handles all of the posted messages by sending the message to
 * the standard output.
 *
 ******************************************************************************/
void AdvisoryStdErr::post(Advisory::Type type, const char *text )
{
    fprintf(stderr,"%s%s\n", TypeName[type], text);
}
