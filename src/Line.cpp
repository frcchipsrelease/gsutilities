#include "gsutilities/Line.h"

using namespace gsu;

double Line::operator()(double x) const
{
    return this->evaluate(x);
}
