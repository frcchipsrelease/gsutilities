/***************************************************************************//**
 *
 * @File PiecewiseBezier.cpp
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *   This is a copy of PiecewiseLinear except the evaluate() method will
 *   calculate a Quadradic Bezier curve for the start and end of segments
 *   to create a smooth transition between segments.
 *
 *   The Bezier portion is based on an example at:
 *     https://stackoverflow.com/questions/785097/how-do-i-implement-a-b%C3%A9zier-curve-in-c
 *
 *   @TODO: Make this a subclass of PiecewiseLinear to reduce duplicate code
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 * 
 ******************************************************************************/
#include "gsutilities/PiecewiseBezier.h"
#include <cmath>

namespace gsu
{

/***************************************************************************//**
 *
 * This no argument constructor is provided so an instance of this class can
 * be created before the curve data is defined. The no argument constructor
 * will create a Piecewise linear curve with 1 segment that will always result
 * in the evaluated value returning the argument to evaluate.
 *
 ******************************************************************************/
PiecewiseBezier::PiecewiseBezier(void)
{
	setCurve(std::vector<double>(), std::vector<double>());
    m_bezier_percent = 0.25;
    m_limit_ends = false;
}

/***************************************************************************//**
 *
 * Create an instance of a PiecewiseBezier class that can be used to
 * evaluate data points.
 *
 * The user must make sure there are at least 2 points
 * The user must make sure points are in order from smallest x to largest x
 *
 * @param	x		- an array of the x values of the points
 * @param	y		- an array of the y values of the points
 * @param   points	- the number of points that define the curve
 *
 ******************************************************************************/
PiecewiseBezier::PiecewiseBezier(double x[], double y[], uint16_t points)
{
	setCurve(x, y, points);
    m_bezier_percent = 0.25;
    m_limit_ends = false;
}

/***************************************************************************//**
 *
 * Create an instance of a PiecewiseBezier class that can be used to
 * evaluate data points.
 *
 * The user must make sure there are at least 2 points
 * The user must make sure points are in order from smallest x to largest x
 *
 * @param	x		- a vector of the x values of the points
 * @param	y		- a vector of the y values of the points
 *
 ******************************************************************************/
PiecewiseBezier::PiecewiseBezier(const std::vector<double>& x, const std::vector<double>& y)
{
	setCurve(x, y);
    m_bezier_percent = 0.25;
    m_limit_ends = false;
}

/***************************************************************************//**
 *
 * A copy constructor that will allow for a deep copy of a PiecewiseBezier
 * object.
 *
 * @param rh	- a reference to the right hand side of the assignment (the 
 *				  object to copy).
 *
 ******************************************************************************/
PiecewiseBezier::PiecewiseBezier(const PiecewiseBezier &rh)
{
	m_points = rh.m_points;
	m_points_x = rh.m_points_x;
	m_points_y = rh.m_points_y;
    m_bezier_percent = rh.m_bezier_percent;
    m_limit_ends = rh.m_limit_ends;
}

/***************************************************************************//**
 *
 * Release the arrays used to hold the points on the curve.
 *
 ******************************************************************************/
PiecewiseBezier::~PiecewiseBezier(void)
{
	m_points_x.clear();
	m_points_y.clear();
	m_points = 0;
}

/***************************************************************************//**
 *
 * Set the curve with two arrays. This method creates two vectors and calls
 * the setCurve() method that takes two vectors.
 *
 * The user must make sure the x and y arrays have the same number of points.
 *
 * The user must make sure the points argument correctly identifies the number
 * of points in the arrays
 *
 * The user must make sure the values of x are in order from smallest to
 * largest
 *
 * @param   x       the array of x values
 * @param   y       the array of y values
 * @param   points  the number of points in the arrays/curve
 *
 ******************************************************************************/
void PiecewiseBezier::setCurve(double x[], double y[], uint16_t points)
{
	setCurve(std::vector<double>(x, x + points), std::vector<double>(y, y + points));
}

/***************************************************************************//**
 *
 * Set the curve with two vectors.
 *
 * The curve must have at least three points. This method will add
 * as many points as needed while attempting to represent the curve that
 * was specified.
 *
 * If empty vectors are specified, a one segment curve will be created that
 * always results in evaluate return the same value that is specified as
 * an argument.
 *
 * If vectors with 1 point are specified, a one segment curve will be created
 * that always results in evaluate returning the value in the y vector.
 *
 * If vectors with 2 points are specified a midpoint will be added to create
 * two segments that are colinear and have the endpoints of the original.
 *
 * The user must make sure the x and y arrays have the same number of points.
 *
 * The user must make sure the values of x are in order from smallest to
 * largest
 *
 * @param   x       the array of x values
 * @param   y       the array of y values
 *
 ******************************************************************************/
void PiecewiseBezier::setCurve(const std::vector<double>& x, const std::vector<double>& y)
{
	m_points = (uint16_t) x.size();
	if (m_points == 0)
	{
        double temp_x[] = { 0.0, 1.0, 2.0 };
        double temp_y[] = { 0.0, 0.0, 0.0 };

        m_points = 3;
        m_points_x = std::vector<double>(temp_x, temp_x + m_points);
        m_points_y = std::vector<double>(temp_y, temp_y + m_points);
	}
    else if (m_points == 1)
    {
        double temp_x[] = { x[0], x[0] + 1.0, x[0] + 2.0 };
        double temp_y[] = { y[0], y[0],       y[0] };

        m_points = 3;
        m_points_x = std::vector<double>(temp_x, temp_x + m_points);
        m_points_y = std::vector<double>(temp_y, temp_y + m_points);
    }
    else if (m_points == 2)
    {
        double temp_x[] = { x[0], (x[0] + x[1]) / 2.0, x[1] };
        double temp_y[] = { y[0], (y[0] + y[1]) / 2.0, y[1] };

        m_points = 3;
        m_points_x = std::vector<double>(temp_x, temp_x + m_points);
        m_points_y = std::vector<double>(temp_y, temp_y + m_points);
    }
    else
	{
		m_points_x = x;
		m_points_y = y;
	}
    
}

/***************************************************************************//**
 *
 * Change the x and y values of one point on the curve.
 *
 * @param   index   the index of the point, from 0 to the the number of
 *                  points - 1, if the index is outside of the allowable
 *                  range this call will do nothing.
 *
 * @param   x_val   the new x value for the point. The value must be between
 *                  the x value of the previous point and the next point, if
 *                  it is not this call will do nothing.
 *
 * @param   y_val   the new y value for this point.
 *
 * @return true if the point is updated
 *
 ******************************************************************************/
bool PiecewiseBezier::setCurvePoint(uint16_t index, double x_val, double y_val)
{
    if (index < m_points)
    {
        if (index == 0)
        {
            if (x_val < m_points_x[1])
            {
                m_points_x[0] = x_val;
                m_points_y[0] = y_val;
                return true;
            }
        }
        else if (index == m_points - 1)
        {
            if (x_val > m_points_x[m_points - 2])
            {
                m_points_x[m_points - 1] = x_val;
                m_points_y[m_points - 1] = y_val;
                return true;
            }
        }
        else
        {
            if ((x_val > m_points_x[index - 1] ) && (x_val < m_points_x[m_points + 1]))
            {
                m_points_x[index] = x_val;
                m_points_y[index] = y_val;
                return true;
            }
        }
    }
    return false;
}

/***************************************************************************//**
 *
 * Set the end points to be limits. In otherwords, if asked to evaluate a
 * point that is less than the first point in the curve, the value of the
 * first point in the curve will be returned, if asked to evaluate a point
 * that is greater than the last point in the curve, the value of the last
 * point in the curve will be returned.
 *
 * @param   limit   if true, evaluation of points ourside of the curve will
 *                  result in the endpoints of the curve, if false the
 *                  last segements of the curve will be extended linearly
 *                  to infinity. The internal value for this is initialized
 *                  as false, the default when calling this method is true.
 *
 ******************************************************************************/
void PiecewiseBezier::setLimitEnds(bool limit)
{
    m_limit_ends = limit;
}

/***************************************************************************//**
 *
 * Sets what percentage of the end of each segment is used for the Bezier
 * smoothing of the transitions between segments.
 *
 * If this method is not called, the default of 25 percent will be used for
 * this instance.
 *
 * @parem percent  The new percentage from 1 to 49. Values outside of this
 *                 range will be limitted to this range.
 *
 ******************************************************************************/
void PiecewiseBezier::setBezierPercent(uint8_t percent)
{
    m_bezier_percent = percent;
    if (percent <= 1) // min percent of segment
    {
        m_bezier_percent = 0.01;
    }
    else if (percent > 49) // max percent of segment
    {
        m_bezier_percent = 0.49;
    }
    else
    {
        m_bezier_percent = percent / 100.0;
    }
}


/***************************************************************************//**
 *
 * Get a value that is the specified percent of the distance between
 * the two values.
 *
 * @param   val1    the first value
 * @param   val2    the the second value
 * @param   percent the percent of the distance the returned value should be
 *                  from the first value.
 *
 * @return a value that is the specified percent of the distance between the
 *         two values away from the first value toward the second value
 *
 ******************************************************************************/
inline double PiecewiseBezier::getPercent( double val1 , double val2 , double percent) const
{
    return val1 + ( (val2 - val1) * percent );
}

/***************************************************************************//**
 *
 * Evaluate a single point on the curve.
 *
 * @param x	- the value to evalueate
 *
 * @return the y value for the given x
 *
 ******************************************************************************/
double PiecewiseBezier::evaluate(double x) const
{
    if (m_limit_ends)
    {
        if (x < m_points_x[0])
        {
            return m_points_y[0];
        }
        else if ( x > m_points_x[m_points - 1])
        {
            {
                return m_points_y[m_points - 1];
            }
        }
    }

    // The first part of the first segment
    // (this prevents an index out of bounds first time through the loop)
    if (x < getPercent(m_points_x[0], m_points_x[1], m_bezier_percent))
    {
        return evaluate(x, m_points_x[0], m_points_y[0], m_points_x[1], m_points_y[1]);
    }

    for (int i = 0; i < (m_points - 2); i++)
    {
        // The first part of the segment
        if (x < getPercent(m_points_x[i], m_points_x[i+1], m_bezier_percent))
        {
            //
            // In much of this we only need the y values, also, calculations for values
            // that are only used once are just substituded into the next calculation
            // instead of the expense of creating a variable and assigning a value
            //

            // Start with three points
            //   - p0 is on the segment from i-1 to i at bezier percent before p1
            //   - p1 is the point the two segments meet (m_points_x[i], p_points_y[i])
            //   - p2 is on the segment from i to i+1 at bezier percent after p1
            //
            double p0_x = getPercent(m_points_x[i-1], m_points_x[i], (1.0 - m_bezier_percent));

            // Find the percentage that x is between P0_x and P2_x
            // p = (x - p0_x) / (p2_x - p0_x);
            double p = (x - p0_x) / (getPercent(m_points_x[i], m_points_x[i+1], m_bezier_percent) - p0_x);

            // Find points Pa and Pb on the segments the calculated percent between the end points
            // pa_y = getPercent( p0_y, p1_y, p)
            // pb_y = getPercent( p1_y, p2_y, p)

            // Find the value of the segment from pa to pb at the point x
            // getPercent( pa_y, pb_y, p)
            return getPercent(
                getPercent( getPercent(m_points_y[i-1], m_points_y[i], (1.0 - m_bezier_percent)), m_points_y[i], p ),
                getPercent( m_points_y[i], getPercent(m_points_y[i], m_points_y[i+1], m_bezier_percent), p ),
                p );
        }

        // The middle part of the segment
        else if (x < getPercent(m_points_x[i], m_points_x[i+1], (1.0 - m_bezier_percent)))
        {
            return evaluate(x, m_points_x[i], m_points_y[i], m_points_x[i+1], m_points_y[i+1]);
        }

        // The last part of the segment
        else if ( x < m_points_x[i+1])
        {
            // p1_x == m_points_x[i+1]
            double p0_x = getPercent(m_points_x[i], m_points_x[i+1], (1.0 - m_bezier_percent));

            double p = (x - p0_x) / (getPercent(m_points_x[i+1], m_points_x[i+2], m_bezier_percent) - p0_x);

            return getPercent(
                getPercent( getPercent(m_points_y[i], m_points_y[i+1], (1.0 - m_bezier_percent)), m_points_y[i+1], p),
                getPercent( m_points_y[i+1], getPercent(m_points_y[i+1], m_points_y[i+2], m_bezier_percent), p),
                p);
        }
    }

    // The first part of the last segment
    if (x < getPercent(m_points_x[m_points - 2], m_points_x[m_points - 1], m_bezier_percent))
    {
        // p1_x == m_points_x[m_points - 2]
        double x0 = getPercent(m_points_x[m_points - 3], m_points_x[m_points - 2], (1.0 - m_bezier_percent));

        double p = (x - x0) / (getPercent(m_points_x[m_points - 2], m_points_x[m_points - 1], m_bezier_percent) - x0);

        return getPercent(
            getPercent(getPercent(m_points_y[m_points - 3], m_points_y[m_points - 2], (1.0 - m_bezier_percent)), m_points_y[m_points-2] , p ) ,
            getPercent(m_points_y[m_points-2], getPercent(m_points_y[m_points - 2], m_points_y[m_points - 1], m_bezier_percent), p ) ,
            p);
    }

    // The middle and last part of the last segment
    return evaluate(x, m_points_x[m_points - 2], m_points_y[m_points - 2], m_points_x[m_points - 1], m_points_y[m_points - 1]);
}

/***************************************************************************//**
 *
 * This method is used to evaluate a point on a given line segment.
 * It does not use any class data so it can be called for any two
 * given points.
 *
 * @param x		- the value to evaluate
 * @param x0	- the x value of the first point that defines the segment
 * @param y0	- the y value of the first point that defines the segment
 * @param x1	- the x value of the second point that defines the segment
 * @param y1	- the y value of the second point that defines the segment
 *
 * @return the y value for the provided x
 *
 ******************************************************************************/
inline double PiecewiseBezier::evaluate(double x, double x0, double y0, double x1, double y1)
{
	// y = (m * x) + b
	// m = (y1 - y0) / (x1 - x0)
	// b = y0 - (m * x0)
	// y = (m * x) + y0 - (m * x0)
	//   = (m * (x - x0)) + y0
	//   = (((y1 - y0) / (x1 - x0)) * (x - x0)) + y0;
	return (((y1 - y0) / (x1 - x0)) * (x - x0)) + y0;
}

}

