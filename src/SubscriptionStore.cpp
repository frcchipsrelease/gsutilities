/*******************************************************************************
 *
 * File: SubscriptionStore.cpp
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *   This is based on code originally from Willow Garage, Inc.
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 * 
 ******************************************************************************/
#include "gsutilities/SubscriptionStore.h"

namespace gsu
{

/***************************************************************************//**
 *
 * Instantiate the SubsriptionStore's container for holding publishers.
 *
 ******************************************************************************/
std::map<std::string, SSPublisherBase *> SubscriptionStore::m_publishers;

uint32_t SubscriptionStore::m_last_subscription = VOID_SUBSCRIPTION_ID;

/***************************************************************************//**
 *
 ******************************************************************************/
void SubscriptionStore::unsubscribe(uint32_t subscription_id)
{
	for (auto publisher : m_publishers)
	{
		publisher.second->unsubscribe(subscription_id);
	}
}

/***************************************************************************//**
 *
 ******************************************************************************/
uint32_t SubscriptionStore::getNextSubscriptionId(void)
{
	m_last_subscription++;
	return (m_last_subscription);
}

/***************************************************************************//**
 *
 ******************************************************************************/
void SubscriptionStore::listPublishers(void)
{
	for (auto publisher : m_publishers)
	{
		printf("%s[%s]\n", publisher.first.c_str(), publisher.second->toString().c_str());
	}
}


}
