/*******************************************************************************
 *
 * File: BuildVersion.cpp
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 * 
 ******************************************************************************/
#include "gsutilities/BuildVersion.h"

#include <stdio.h>

namespace gsu
{

uint16_t BuildVersion::c_major = 0;
uint16_t BuildVersion::c_minor = 0;
uint16_t BuildVersion::c_patch = 0;
uint16_t BuildVersion::c_build = 0;

/*******************************************************************************
 *
 ******************************************************************************/
void BuildVersion::setVersion(uint16_t major, uint16_t minor, uint16_t patch, uint16_t build)
{
	c_major = major;
	c_minor = minor;
	c_patch = patch;
	c_build = build;
}

	
/*******************************************************************************
 *
 ******************************************************************************/
uint16_t BuildVersion::getMajor(void)
{ 
	return c_major;
}

/*******************************************************************************
 *
 ******************************************************************************/
uint16_t BuildVersion::getMinor(void)
{
	return c_minor;
}

/*******************************************************************************
*
******************************************************************************/
uint16_t BuildVersion::getPatch(void)
{
	return c_patch;
}

/*******************************************************************************
 *
 ******************************************************************************/
uint16_t BuildVersion::getBuild(void)
{
	return c_build;
}

/*******************************************************************************
 *
 ******************************************************************************/
std::string BuildVersion::getVersionStr(void)
{
	char ver[32];
	sprintf(ver, "%04d.%02d.%02d-%04d", getMajor(), getMinor(), getPatch(), getBuild());
	return ver;
}

}
