/*******************************************************************************
 *
 * File LineFactory.cpp
 *
 * Written by:
 *  Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/
#include <math.h>
#include <cstring>
#include <sstream>
#include <string>
#include <algorithm>

#include "gsutilities/LineFactory.h"
#include "gsutilities/Advisory.h"

using namespace gsu;

Line* LineFactory::createLine( tinyxml2::XMLElement* xml )
{
    Line* ret = nullptr;
    const char *type = xml->Attribute("type");
    if(strcmp("polynomial", type) == 0)
    {
        Advisory::pwarning("Entering create polynomial.");
        ret = createPolynomial( xml );
    }
    else if(strcmp("spline", type) == 0)
    {
        ret = createSpline( xml );
    }
    else if(strcmp("piecewise_linear", type) == 0)
    {
        ret = createPiecewiseLinear( xml );
    }
    else if(strcmp("piecewise_bezier", type) == 0)
    {
        ret = createPiecewiseBezier( xml );
    }
    else
    {
        Advisory::pinfo("    failed to create line, unknown type \"%s\"", type);
    }
    return ret;
}

Polynomial* LineFactory::createPolynomial(tinyxml2::XMLElement* xml)
{
    tinyxml2::XMLElement* comp = nullptr;
    const char* value_string = nullptr;
    Polynomial* ret = nullptr;

    // 1) check for delivered coefficients attribute
    if(( value_string = xml->Attribute("coefficients") ))
    {
        std::vector<double> coeffs;
        double c_i;

        // Read input string to stream
        std::string str = value_string;
        std::replace(str.begin(), str.end(), ',', ' ');
        std::stringstream ss(str);

        // Create coefficients
        while(ss>>c_i)
        {
            coeffs.push_back(c_i);
        }
        if( coeffs.empty() )
        {
            coeffs.push_back(0.0);
        }
        // Read it back for verification
        ss.str(std::string());
        ss.clear();
        ss << coeffs[0];
        for(unsigned ii = 1; ii < coeffs.size(); ii++)
        {
            ss << " + " << coeffs[ii] << "*x^" << ii;
        }
        Advisory::pwarning("    Creating Polynomial: %s", ss.str().c_str() );
        // Construct
        ret = new Polynomial(coeffs);
    }
    // 2) Check for child coefficient attributes
    else if(( comp = xml->FirstChildElement("coefficient") ))
    {
        std::vector<int> order;
        std::vector<double>  coeffs;
        int o_i;
        double c_i;
        static int count = 0;
        while( comp != nullptr)
        {
            int status_order = comp->QueryIntAttribute("order", &o_i);
            int status_value = comp->QueryDoubleAttribute("value", &c_i);
            if(    status_order
               ||  status_value )
            {
                Advisory::pwarning("%d Failed polynomial child coefficient parse "
                                   "{order, value} = {%d, %f}", count++, o_i, c_i);
                Advisory::pwarning("status_order = %d", status_order );
                Advisory::pwarning("status_value = %d", status_value );                
            }
            else
            {
                order.push_back(o_i);
                coeffs.push_back(c_i);
            }
            comp = comp->NextSiblingElement("coefficient");
        }
        Advisory::pwarning("Left the loop.");
        if( order.empty() )
        {
            Advisory::pwarning("Read empty polynomial. Creating default line y = x");
            order.push_back(0);
            order.push_back(1);
            coeffs.push_back(0.0);
            coeffs.push_back(1.0);
            o_i = 1;
        }
        else
        {
            o_i = *std::max_element( order.begin(), order.end() );
        }
        std::vector<double> ordered_coeffs(o_i+1,0.0);
        for(unsigned ii = 0; ii < coeffs.size(); ii++)
        {
            ordered_coeffs[order[ii]] = coeffs[ii];
        }
        // Read it back for verification
        std::stringstream ss;
        ss << ordered_coeffs[0];
        for(unsigned ii = 1; ii < ordered_coeffs.size(); ii++)
        {
            ss << " + " << ordered_coeffs[ii] << "*x^" << ii;
        }
        Advisory::pwarning("    Creating Polynomial: %s", ss.str().c_str() );
        ret = new Polynomial(ordered_coeffs);
    }
    // 3) Check for child points
    else if(( comp = xml->FirstChildElement("point") ))
    {
        std::vector<double> x;
        std::vector<double> y;
        double x_i = 0.0;
        double y_i = 0.0;
        while( comp != nullptr)
        {
            if(   xml->QueryDoubleAttribute("x", &x_i)
               || xml->QueryDoubleAttribute("y", &x_i) )
            {
                Advisory::pwarning("  Failed polynomial child point parse");
            }
            x.push_back(x_i);
            y.push_back(y_i);
            comp = comp->NextSiblingElement("point");
        }
        if( x.empty() )
        {
            x.push_back(0.0);
            y.push_back(0.0);
        }
        // Read it back for verification
        std::stringstream ss;
        for(unsigned ii = 0; ii < x.size(); ii++)
        {
            ss << "{ " << x[ii] << ", " << y[ii] << "} ";
        }
        Advisory::pwarning("    Creating Polynomial: %s", ss.str().c_str() );
        ret = new Polynomial(x,y);
    }
    else
    {
        Advisory::pwarning("    ! Creating blank Polynomial");
        ret = new Polynomial();
    }

    return ret;
}

Spline* LineFactory::createSpline(tinyxml2::XMLElement* xml)
{
    tinyxml2::XMLElement* comp = nullptr;
    Spline* ret = nullptr;

    // 1) Check for child points
    if(( comp = xml->FirstChildElement("point") ))
    {
        std::vector<double> x;
        std::vector<double> y;
        double x_i = 0.0;
        double y_i = 0.0;
        while( comp != nullptr)
        {
            if(   xml->QueryDoubleAttribute("x", &x_i)
               || xml->QueryDoubleAttribute("y", &x_i) )
            {
                Advisory::pwarning("  Failed spline point parse");
            }
            x.push_back(x_i);
            y.push_back(y_i);
            comp = comp->NextSiblingElement("point");
        }
        if( x.empty() )
        {
            x.push_back(0.0);
            y.push_back(0.0);
        }
        // Read it back for verification
        std::stringstream ss;
        for(unsigned ii = 0; ii < x.size(); ii++)
        {
            ss << "{ " << x[ii] << ", " << y[ii] << "} ";
        }
        Advisory::pwarning("    Creating Spline: %s", ss.str().c_str() );
        ret = new Spline(x,y);
    }
    else
    {
        Advisory::pwarning("    ! Creating blank Spline");
        ret = new Spline();
    }

    return ret;
}

PiecewiseLinear* LineFactory::createPiecewiseLinear(tinyxml2::XMLElement* xml)
{
    tinyxml2::XMLElement* comp = nullptr;
    PiecewiseLinear* ret = nullptr;

    // 1) Check for child points
    if(( comp = xml->FirstChildElement("point") ))
    {
        std::vector<double> x;
        std::vector<double> y;
        double x_i = 0.0;
        double y_i = 0.0;
        while( comp != nullptr)
        {
            if(   xml->QueryDoubleAttribute("x", &x_i)
               || xml->QueryDoubleAttribute("y", &x_i) )
            {
                Advisory::pwarning("  Failed PiecewiseLinear point parse");
            }
            x.push_back(x_i);
            y.push_back(y_i);
            comp = comp->NextSiblingElement("point");
        }
        if( x.empty() )
        {
            x.push_back(0.0);
            y.push_back(0.0);
        }
        // Read it back for verification
        std::stringstream ss;
        for(unsigned ii = 0; ii < x.size(); ii++)
        {
            ss << "{ " << x[ii] << ", " << y[ii] << "} ";
        }
        Advisory::pwarning("    Creating PiecewiseLinear: %s", ss.str().c_str() );
        ret = new PiecewiseLinear(x,y);
    }
    else
    {
        Advisory::pwarning("    ! Creating blank PiecewiseLinear");
        ret = new PiecewiseLinear();
    }

    bool limit = false;
    xml->QueryBoolAttribute("limit", &limit);
    ret->setLimitEnds(limit);

    return ret;
}

PiecewiseBezier* LineFactory::createPiecewiseBezier(tinyxml2::XMLElement* xml)
{
    tinyxml2::XMLElement* comp = nullptr;
    PiecewiseBezier* ret = nullptr;

    // 1) Check for child points
    if(( comp = xml->FirstChildElement("point") ))
    {
        std::vector<double> x;
        std::vector<double> y;
        double x_i = 0.0;
        double y_i = 0.0;
        while( comp != nullptr)
        {
            if(   xml->QueryDoubleAttribute("x", &x_i)
               || xml->QueryDoubleAttribute("y", &x_i) )
            {
                Advisory::pwarning("  Failed PiecewiseBezier point parse");
            }
            x.push_back(x_i);
            y.push_back(y_i);
            comp = comp->NextSiblingElement("point");
        }
        if( x.empty() )
        {
            x.push_back(0.0);
            y.push_back(0.0);
        }
        // Read it back for verification
        std::stringstream ss;
        for(unsigned ii = 0; ii < x.size(); ii++)
        {
            ss << "{ " << x[ii] << ", " << y[ii] << "} ";
        }
        Advisory::pwarning("    Creating PiecewiseBezier: %s", ss.str().c_str() );
        ret = new PiecewiseBezier(x,y);
    }
    else
    {
        Advisory::pwarning("    ! Creating blank PiecewiseBezier");
        ret = new PiecewiseBezier();
    }

    bool limit = false;
    xml->QueryBoolAttribute("limit", &limit);
    ret->setLimitEnds(limit);

    return ret;
}
