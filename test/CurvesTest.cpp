#include <stdio.h>
#include <stdlib.h>

#include "gsutilities/Polynomial.h"
#include "gsutilities/PiecewiseLinear.h"
#include "gsutilities/PiecewiseBezier.h"
#include "gsutilities/Spline.h"


using namespace gsu;

int main(int argc, char **argv)
{
    FILE* data_file = fopen("../curves_test_data.csv", "w");

    printf("***** gsUtilities Curves test - start *****\n");

    PiecewiseLinear pwl01;
    PiecewiseBezier pwb01;
    PiecewiseBezier pwb02;
    Spline spl01;

    pwl01.setCurve(
        std::vector<double>{0.0, 10.0, 20.0, 30.0, 40.0, 50.0, 60.0  },
        std::vector<double>{0.0, 0.0, 10.0, 10.0, 0.0, 40.0, 00.0});
    pwl01.setLimitEnds(true);

    pwb01.setCurve(
        std::vector<double>{0.0, 10.0, 20.0, 30.0, 40.0, 50.0, 60.0  },
        std::vector<double>{0.0, 0.0, 10.0, 10.0, 0.0, 40.0, 0.0});
    pwb01.setLimitEnds(true);
    pwb01.setBezierPercent(25);

    pwb02.setCurve(
        std::vector<double>{0.0, 10.0, 20.0, 30.0, 40.0, 50.0, 60.0  },
        std::vector<double>{0.0, 0.0, 10.0, 10.0, 0.0, 40.0, 0.0});
    pwb02.setLimitEnds(true);
    pwb02.setBezierPercent(45);

    spl01.setCurve(
        std::vector<double>{0.0, 10.0, 20.0, 30.0, 40.0, 50.0, 60.0  },
        std::vector<double>{0.0, 0.0, 10.0, 10.0, 0.0, 40.0, 00.0});

    for (double x = 0.0; x <= 60.0; x+= 0.1)
    {
        fprintf(data_file, "%8.3f, %8.3f, %8.3f, %8.3f, %8.3f\n",
            x, pwl01.evaluate(x), pwb01.evaluate(x), pwb02.evaluate(x), spl01.evaluate(x));
        fflush(data_file);
    }

     printf("***** gsUtilities Curves test - complete *****\n");
    fclose(data_file);
}
