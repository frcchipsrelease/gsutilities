/*******************************************************************************
 *
 * File: HardwareFactory.h
 *
 * Written by:
 *  Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "gsutilities/tinyxml2.h"

#include "gsutilities/Line.h"
#include "gsutilities/PiecewiseBezier.h"
#include "gsutilities/PiecewiseLinear.h"
#include "gsutilities/Polynomial.h"
#include "gsutilities/Spline.h"

/*******************************************************************************
 *
 * This class contains methods for parsing XML elements to create objects
 * for use by the robot.  This is intended to help keep the XML interface
 * consistent across many classes and to reduce the amount of duplicate
 * code.
 *
 ******************************************************************************/
class LineFactory
{
    public:
        static gsu::Line*            createLine(            tinyxml2::XMLElement* xml);
        static gsu::Polynomial*      createPolynomial(      tinyxml2::XMLElement* xml);
        static gsu::Spline*          createSpline(          tinyxml2::XMLElement* xml);
        static gsu::PiecewiseLinear* createPiecewiseLinear( tinyxml2::XMLElement* xml);
        static gsu::PiecewiseBezier* createPiecewiseBezier( tinyxml2::XMLElement* xml);
};
