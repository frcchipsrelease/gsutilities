/*********************************************************************
 *
 * File: Angles.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *   This is based on code originally from Willow Garage, Inc.
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 * 
 *********************************************************************/
#pragma once

#include <algorithm>
#include <math.h>
#include <stdio.h>

namespace gsu
{
	class Angle
	{
        public:
            constexpr static const double PI = 3.141592653589793238463;

			template<typename Typ>
			static inline Typ toRadians(Typ degrees) 
			{
                return degrees * (Typ)(PI / 180.0);
			}

			template<typename Typ>
			static inline Typ toDegrees(Typ radians) 
			{
                return radians * (Typ)(180.0 / PI);
			}

			template<typename Typ>
			static inline Typ normalizePositive(Typ angle) 
			{
                return (Typ)fmod(fmod(angle, (Typ)(2.0*PI)) + (Typ)(2.0*PI), (Typ)(2.0*PI));
			}

			template<typename Typ>
			static inline Typ normalize(Typ angle) 
			{
				Typ a = normalizePositive(angle);
                if (a > (Typ)PI)
                a -= (Typ)(2.0 *PI);
				return a;
			}

			template<typename Typ>
			static inline Typ shortestDistance(Typ from, Typ to) 
			{
				Typ result = normalizePositive(normalizePositive(to) - normalizePositive(from));

                if (result > PI)
				{
                    result = -((Typ)(2.0*PI) - result);
				}
				
				return normalize(result);
			}
    };

} // namespace gsu

