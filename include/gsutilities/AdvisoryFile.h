/*******************************************************************************
 *
 * File: AdvisoryFile.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "gsutilities/Advisory.h"
#include <stdio.h>

/*******************************************************************************
 *
 * The AdvisoryFile class will handle all messages by sending them 
 * to the provided file.
 *
 ******************************************************************************/
class AdvisoryFile : public Advisory
{
	public:
		AdvisoryFile(FILE *file_ptr);
		~AdvisoryFile();

		void post(Type type, const char *text);

	private:
        FILE * m_file_ptr;
};
