/*******************************************************************************
 *
 * File: AdvisoryStdOut.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "gsutilities/Advisory.h"

/*******************************************************************************
 *
 * The AdvisoryStdOut class will print all messages to standard out
 *
 ******************************************************************************/
class AdvisoryStdOut : public Advisory
{
	public:
		AdvisoryStdOut(void);
		~AdvisoryStdOut(void);

        void post(Type type, const char *text );
};


