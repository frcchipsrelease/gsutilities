/***************************************************************************//**
 *
  File:PiecewiseBezier.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "gsutilities/Line.h"

namespace gsu 
{

/***************************************************************************//**
 *
 * This class implents a Piecewise function that is constructed by connecting
 * linear segments between points. Then a Qudradic Bezier curve is used to
 * smooth the transition between segments. How much smoothing is based on
 * the Bezier Percent. This percent can be set to values ranging from 5% (least
 * smoothing) to 45% (most smoothing), the percentage dictates what percent of
 * the segment will be replaced with the Bezier curve on each end of the segment.
 * The remaining middle section of the segment will be a linear interpulation.
 *
 ******************************************************************************/
class PiecewiseBezier : public Line
{
    public:
        PiecewiseBezier(void);
        PiecewiseBezier(double x[], double y[], uint16_t points);
        PiecewiseBezier(const std::vector<double>& x, const std::vector<double>& y);
        PiecewiseBezier(const PiecewiseBezier &rh);
        virtual ~PiecewiseBezier(void);

        void setCurve(double x[], double y[], uint16_t points);
        void setCurve(const std::vector<double>& x, const std::vector<double>& y);

        bool setCurvePoint(uint16_t index, double x_val, double y_val);

        void setLimitEnds(bool limit=true);
        void setBezierPercent(uint8_t percent);

        double evaluate(double x) const;
        static double evaluate(double x, double x0, double y0, double x1, double y1);

    private:
        double getPercent( double val1 , double val2 , double percent) const;

        uint16_t m_points;
        std::vector<double> m_points_x;
        std::vector<double> m_points_y;

        double m_bezier_percent;
        bool m_limit_ends;
};

}
