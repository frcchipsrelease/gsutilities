/***************************************************************************//**
 *
  File:Line.h
 *  Generic Software Utilities
 *
 * Written by:
 *  Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <stdint.h>
#include <vector>

namespace gsu
{

/******************************************************************************
 * Implements an abstract line base class for inheritance
 ******************************************************************************/
class Line
{
    public:
        Line(void) = default;
        virtual ~Line() = default;

        virtual void setCurve(double x[], double y[], uint16_t points) = 0;
        virtual void setCurve(const std::vector<double>& x, const std::vector<double>& y) = 0;

        virtual double evaluate(double x) const = 0;
        double operator() (double x) const;
};

}
