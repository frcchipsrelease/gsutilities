/*******************************************************************************
 *
 * File: SegmentedFile.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <string>

/*******************************************************************************
 *
 ******************************************************************************/
namespace gsu
{
class SegmentedFile
{
    public:
        SegmentedFile(std::string path, std::string name, std::string ext, uint32_t max_segments);
        virtual ~SegmentedFile(void);

        bool createPath(void);

        std::string getPath(void);
        void setPath(std::string path);

        std::string getName(void);
        void setName(std::string name);

        std::string getExtension(void);
        void setExtension(std::string ext);

        uint32_t getMaxSegments(void);
        void setMaxSegments(uint32_t max);
        
        int32_t  getCurrentIndex(void);

        virtual FILE *openNextSegment(std::string aux_info=std::string(""));
        std::string getSegmentName(uint16_t index, std::string aux_info=std::string(""));

    protected:
        uint16_t getLastSegmentIndex(void);
        void	 putNextSegmentIndex(uint16_t index);

        std::string getIndexFileName(void);
    private:
        std::string m_path;
        std::string m_name;
        std::string m_extension;

        int32_t m_current_index;
        uint32_t m_max_segments;
};
}
