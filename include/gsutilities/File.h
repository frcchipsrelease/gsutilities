/*******************************************************************************
 *
 * File: File.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <string>
#include <stdio.h>
#include <sys/stat.h>
#include <stdexcept>
#include <fstream>
#include <iostream>

/*******************************************************************************
 *
 * This class contains static methods that aid in finding certain file
 * properties
 *
 ******************************************************************************/

namespace gsu
{
class File
{
    public:
        static void create(std::string path, std::fstream &file_stream);
        static bool exists(std::string path);
        static void open(std::string path, std::fstream& file_stream);
        static bool remove(std::string path);
};
}
