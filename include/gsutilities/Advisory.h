/*******************************************************************************
 *
 * File: Advisory.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

class AdvisoryBuffer;  // pre-declare this class to help reolve circular refernce

#include "gsutilities/RecycleQueue.hpp"

#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>
#include <thread>
#include <vector>

/*******************************************************************************
 *
 * The Advisory class provides an interface for sending messages to the
 * user. This class by itself does nothing but provide an interface. In order
 * for the messages to get to the user, one of the sub classes must be 
 * initialized to provide handling of the messages. Additional subclasses
 * can be created to provide different options for handling the messages.
 *
 ******************************************************************************/
class Advisory 
{
	public:
		typedef enum {INFO, CAUTION, WARNING, FATAL} Type;

        static void postInfo(const char *fmt, ...);
        static void postCaution(const char *fmt, ...);
        static void postWarning(const char *fmt, ...);
        static void postFatal(const char *fmt, ...);

		// These are old and will be removed some day, use the post functions instead
        static void pinfo(const char *fmt, ...);
        static void pcaution(const char *fmt, ...);
        static void pwarning(const char *fmt, ...);
        static void pfatal(const char *fmt, ...);

        static const char *TypeName[];

		static void addObserver(Advisory *obs);
		
		template <class T> static T * getInstance(void);

		virtual void post(Type type, const char *text) = 0;

	protected:
		Advisory(void);
		virtual ~Advisory(void);

	private:
		static void post_impl(Type type, const char *fmt, va_list vlist);
		static void drain_impl(void);

		static std::vector<Advisory *> m_observers;
		static std::thread drain_thread;
		static gsu::RecycleQueue<AdvisoryBuffer> advisory_queue;
};

/*******************************************************************************
*
* @return the first observer that is an instance of the templated type
*
******************************************************************************/
template<class T> T * Advisory::getInstance(void)
{
	T *instance;

	for (Advisory *obs : m_observers)
	{
		instance = dynamic_cast<T *>(obs);
		if (nullptr != instance)
		{
			return instance;
		}
	}

	return nullptr;
}

/*******************************************************************************
 *
 ******************************************************************************/
class AdvisoryBuffer
{
   public:
      static const uint16_t TEXT_SIZE = 512;

      AdvisoryBuffer(void);

      Advisory::Type type;
      char text[TEXT_SIZE];
};

