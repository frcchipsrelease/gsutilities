/*******************************************************************************
 *
 * File: AdvisoryStdErr.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "gsutilities/Advisory.h"

/*******************************************************************************
 *
 * The AdvisoryStdErr class will print all messages to standard error
 *
 ******************************************************************************/
class AdvisoryStdErr : public Advisory
{
	public:
        AdvisoryStdErr(void);
        ~AdvisoryStdErr(void);

        void post(Type type, const char *text );
};


