/*******************************************************************************
 *
 * File: Polynomial.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <cassert>

#include "gsutilities/Line.h"

namespace gsu 
{

/*******************************************************************************
 *
 ******************************************************************************/
class Polynomial : public Line
{
	public:
		Polynomial(void);
		Polynomial(uint8_t order);
		Polynomial(std::vector<double>& coefficients);
		Polynomial(const std::vector<double>& x, const std::vector<double>& y);
		Polynomial(const Polynomial &rh);
		virtual ~Polynomial(void);

		void setOrder(uint8_t order);
		uint8_t getOrder(void);

		void setCoefficients(std::vector<double>& coefficients);
		void setCoefficient(uint8_t c, double value);
		double getCoefficient(uint8_t c);

		void setCurve(const std::vector<double>& x, const std::vector<double>& y); // do a best fit of these points
		void setCurve(double x[], double y[], uint16_t num_points);  // do a best fit of these points

		double evaluate(double x) const;

	private:
		uint8_t m_order;
		std::vector<double> m_coefficients;
};

}
