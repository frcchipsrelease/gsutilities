/*********************************************************************
 *
 * File: Filter.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *   This is based on code originally from Willow Garage, Inc.
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 * 
 *********************************************************************/
#pragma once

#include <algorithm>
#include <math.h>
#include <stdio.h>

namespace gsu
{
	class Filter
	{
        public:

        static double limit(double value, double min, double max);
        static double step (double value, double size, double target);

		
    };

} // namespace gsu

