/*******************************************************************************
 *
 * File: DataLogger.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District
 *  NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "gsutilities/SegmentedFile.h"
#include "gsutilities/RecycleQueue.hpp"

#include <string>
#include <thread>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

namespace gsu
{

/*******************************************************************************
 *
 * @class DataBuffer
 * 
 * This is a generic buffer that can be used to hold any kind of data. It
 * is really intended to hold blocks of text with some maximum size but 
 * could easily hold anything.
 * 
 ******************************************************************************/
class DataBuffer
{
    public:
        DataBuffer(uint16_t max_size=1024);
        ~DataBuffer(void);

        uint16_t getBufferSize(void);

        uint16_t max_data_size;
        uint8_t* data;
};

/*******************************************************************************
 *
 * @class DataLogger
 * 
 * This class uses the DataBuffer class to hold blocks of text, a 
 * SegmentedFilen to write those blocks of text to a file, a
 * RecycleQueue with a standard thread to buffer the blocks so the 
 * calling thread does not need to wait for file IO which can be slow
 * and unpredictable.
 * 
 ******************************************************************************/
class DataLogger
{
	public:
        DataLogger(std::string path, std::string name, std::string ext, 
            uint8_t max_segments, uint8_t max_buffers, uint16_t buffer_size);
        ~DataLogger(void);

        void open(const char* phase_name);
        void close(void);

        uint16_t getBufferSize(void);
        DataBuffer* getEmptyBuffer(void);
        void putFullBuffer(DataBuffer* buffer);

    private:
        void drainMethod(void);
        
        gsu::RecycleQueue<DataBuffer> queue;
        gsu::SegmentedFile file;
        FILE* file_segment;
        bool logging_done;
        uint16_t max_buffer_size; 
        std::thread drain_thread;
        std::string filename_base;
};

}
