/***************************************************************************//**
 *
  File:PiecewiseLinear.h
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "gsutilities/Line.h"

namespace gsu 
{

/***************************************************************************//**
 *
 ******************************************************************************/
class PiecewiseLinear : public Line
{
    public:
        PiecewiseLinear(void);
        PiecewiseLinear(double x[], double y[], uint16_t points);
        PiecewiseLinear(const std::vector<double>& x, const std::vector<double>& y);
        PiecewiseLinear(const PiecewiseLinear &rh);
        PiecewiseLinear& operator=(const PiecewiseLinear& assign_pl);
        virtual ~PiecewiseLinear(void);

        void setCurve(double x[], double y[], uint16_t points);
        void setCurve(const std::vector<double>& x, const std::vector<double>& y);

        bool setCurvePoint(uint16_t index, double x_val, double y_val);

        void setLimitEnds(bool limit=true);

        double evaluate(double x) const;
        static double evaluate(double x, double x0, double y0, double x1, double y1);

    private:

        uint16_t m_points;
        std::vector<double> m_points_x;
        std::vector<double> m_points_y;

        bool m_limit_ends;
};

}
