/*******************************************************************************
 *
 * File: RecycleQueue.hpp
 *	Generic Software Utilities
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <queue>
#include <chrono>
#include <mutex>
#include <condition_variable>

#include <cstring>
#include <stdint.h>

namespace gsu
{

/*******************************************************************************
 *
 * @class 	RecycleQueue 
 * 
 * This is actually two queues, one holds pointers to objects that are ready 
 * to be consumed, the other holds pointers to objects that are ready to be 
 * filled with new data.
 * 
 * The idea is that the provider gets "empty" objects, fills them with new
 * data, and puts them back into this queue as "full" objects. When filled
 * objects are put into this class all consumers are notified that a new
 * object is available. The consumer(s) then get a full object and use the
 * data before returning that object to this queue as an empty object (they
 * recycle it).
 * 
 * This class can be constructed with two arguments, one specifies the total
 * maximum number of objects in the queue (empty + full), if an attempt to
 * put more objects into the queue is made, the one being put in that violates
 * the maximum size is deleted (full or empty, it doesn't matter). The second
 * option is to automatically create objects if there aren't any in the empty
 * queue when requested, this will allow the number of objects to grow to what
 * is needed (or to the maximum number of objects) but will also allow for dynamic 
 * memory allocation (which might be undesired in a real-time application). If 
 * avoiding dynamic memory allocation, the provider can prefill the empty queue 
 * to the desired size during initialization.
 * 
 ******************************************************************************/
template <typename data_type> class RecycleQueue
{
	public:
		RecycleQueue(uint16_t max_size=3, bool create_objects=true);
		~RecycleQueue(void);

		void putEmpty(data_type* obj);
		data_type* getEmpty(void);

		void putFull(data_type* obj);
		data_type* getFull(int timeout=250);

        uint16_t getFullAvailable(void);
        uint16_t getEmptyAvailable(void);

	private:
		std::queue<data_type*> empty_queue;
		std::queue<data_type*> full_queue;
		std::condition_variable gate;

		std::mutex empty_guard;
		std::mutex full_guard;

		bool create_as_needed;
		uint16_t max_queue_size;
};

/*******************************************************************************
 *
 * @brief	Create an instance of this class
 * 
 * @param	max_size	the maximum total number (empty + full) objects that
 * 						will be managed by this class
 * 
 * @param	create_objects	if true, new instances of the <data_type> objects
 * 							will be created with the no argument constructor
 * 							when attempting to get an empty object while there
 * 							aren't any in the empty queue.
 * 
 ******************************************************************************/
template <typename data_type> RecycleQueue<data_type>::RecycleQueue(uint16_t max_size, bool create_objects)
{
	create_as_needed = create_objects;
	
	if (max_size > 3)
	{	
		max_queue_size = max_size;
	}
	else
	{
		max_queue_size = 3;
	}
}

/*******************************************************************************
 *
 * @brief	Destroy this instance of this class while cleaning up all resorces
 * 
 * This will delete all of the objects being managed by this class (both empty
 * and full), and will set the maximum queue size to zero so no more objects
 * can be put into this instance.
 * 
 ******************************************************************************/
template <typename data_type> RecycleQueue<data_type>::~RecycleQueue(void)
{
	std::lock_guard<std::mutex> elock(empty_guard);
	while( ! empty_queue.empty())
	{
		data_type* ret_obj = empty_queue.front();
		empty_queue.pop();
		delete ret_obj;
	}

	std::lock_guard<std::mutex> flock(full_guard);
	while( ! full_queue.empty())
	{
		data_type* ret_obj = full_queue.front();
		full_queue.pop();
		delete ret_obj;
	}

	max_queue_size = 0;
}

/*******************************************************************************
 * 
 * @return the number of empty objects currently in the queue
 * 
 ******************************************************************************/
template <typename data_type> uint16_t RecycleQueue<data_type>::getEmptyAvailable(void)
{
    std::lock_guard<std::mutex> lock(full_guard);
    return empty_queue.size();
}

/*******************************************************************************
 *
 * @brief	put an instance of <data_type> (by pointer) that is ready to be 
 * 			filled into this queue
 * 
 ******************************************************************************/
template <typename data_type> void RecycleQueue<data_type>::putEmpty(data_type* obj)
{
	if (obj != nullptr)
	{
		if (full_queue.size() + empty_queue.size() < max_queue_size)
		{
			std::lock_guard<std::mutex> lock(empty_guard);
			empty_queue.push(obj);
		}
		else
		{
			delete obj;
		}
	}
}

/*******************************************************************************
 *
 * @return	an instance of <data_type> by pointer that is ready to be filled or
 * 			a null pointer if there are no empty objects in the empty queue and
 *          this object is set to not create new objects OR there was an
 * 			allocation error. This does not wait for an empty to be returned
 *          because the provider normally doesn't want to wait.
 * 
******************************************************************************/
template <typename data_type> data_type* RecycleQueue<data_type>::getEmpty(void)
{
	data_type* ret_obj = nullptr;

	std::lock_guard<std::mutex> lock(empty_guard);

	if (empty_queue.empty())
	{
		if (create_as_needed)
		{
			ret_obj = new data_type();
		}
	}
	else
	{
		ret_obj = empty_queue.front();
		empty_queue.pop();
	}

	return ret_obj;
}

/*******************************************************************************
 * 
 * @return the number of full objects currently in the queue
 * 
 ******************************************************************************/
template <typename data_type> uint16_t RecycleQueue<data_type>::getFullAvailable(void)
{
    std::lock_guard<std::mutex> lock(full_guard);
    return full_queue.size();
}

/*******************************************************************************
 *
 * @brief	put an instance of <data_type> (by pointer) that is ready to be 
 * 			used into this queue
 * 
 ******************************************************************************/
template <typename data_type> void RecycleQueue<data_type>::putFull(data_type* obj)
{
	if (obj != nullptr)
	{
		if (full_queue.size() + empty_queue.size() < max_queue_size)
		{
			{
				std::lock_guard<std::mutex> lock(full_guard);
				full_queue.push(obj);
			}
			gate.notify_all();
		}
		else
		{
			delete obj;
		}
	}
}

/*******************************************************************************
 *
 * @param	timeout	the number of milliseconds that the calling thread is willing
 * 			to wait for an object that is ready to be used. If there is an 
 * 			object ready before this time the function call will return sooner.
 * 
 * @return	an instance of <data_type> by pointer that is ready to be used or
 * 			a null pointer if there are not any ready to use instances before
 *          timeout milliseconds pass
 * 
 ******************************************************************************/
template <typename data_type> data_type* RecycleQueue<data_type>::getFull(int timeout)
{
	data_type* ret_obj = nullptr;

	std::unique_lock<std::mutex> lock(full_guard);
	if (full_queue.empty())
	{
		gate.wait_for(lock, std::chrono::milliseconds(timeout), [&]{return full_queue.size()>0;});
	}

	if ( ! full_queue.empty())
	{
		ret_obj = full_queue.front();
		full_queue.pop();
	}
	return ret_obj;
}

}
